import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
      <div className="content-bg">
        <img className="image-bg" src="assets/images/bg_login.png" alt="bg_login"></img>
      </div>

      <div className="row login-form">
        <div className="col-lg-5">
          <div className="card rounded-lg">
            <div className="row">
              <div className="col-md-12">
                <h5>Login</h5>
                <a href="#" className="float-right link"Belum punya akun></a>
              </div>
              <div className="col-md-12">
                <form>
                  <div className="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" />
                  </div>
                  <div className="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                  </div>
                  <div className="form-group form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label" for="exampleCheck1">Ingat saya</label>
                    <a href="#" className="float-right link">Lupa Password ?</a>
                  </div>
                  <button type="submit" className="btn btn-primary btn-block">Log In</button>
                  <div className="line">
                    <h6><span>atau masuk dengan</span></h6>
                  </div>
                  <button className="btn btn-outline-danger btn-block icon-btn"><i className="fa fa-google-plus"></i> Google</button>
                </form>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  );
}

export default App;
